1.1: Is Unique
======================================

### STATUS:
* master: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/is-unique/badges/master/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/is-unique/commits/master)
* develop: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/is-unique/badges/develop/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/is-unique/commits/develop)

======================================
### Table of Contents
1. [Running Tests](https://gitlab.com/cracking-the-coding-interview/IsUnique#running-tests)
2. [Problem Statement](https://gitlab.com/cracking-the-coding-interview/IsUnique#problem-statement)
3. [Questions](https://gitlab.com/cracking-the-coding-interview/IsUnique#questions)
4. [Solution](https://gitlab.com/cracking-the-coding-interview/IsUnique#solution)
	- [Part 1](https://gitlab.com/cracking-the-coding-interview/IsUnique#part-1)
	- [Part 2](https://gitlab.com/cracking-the-coding-interview/IsUnique#part-2)
5. [Implementation](https://gitlab.com/cracking-the-coding-interview/IsUnique#implementation)
6. [Additional notes](https://gitlab.com/cracking-the-coding-interview/IsUnique#additional-notes)
7. [References](https://gitlab.com/cracking-the-coding-interview/IsUnique#references)

# Running Tests:
I'm using Gulp as a task runner, eslint
(with airbnb coding  standards) for code linting,
and mocha as a testing framework.  To run tests, after a fresh
clone of this repository, run:
```
npm install
gulp test
```
The grunt test task will run the lint and mocha tasks sequentially.  Those tasks will run eslint, and mocha.

# Problem Statement:
Implement an algorithm to determine if a string has all
unique characters.  What if you cannot use additional data
structures?

# Questions:
1. I'd need to ask what an "additional data structure" is.
Does this mean data structures not in the language's standard
library?  Or anything that isn't a primitive?  Or any use
of extra storage space beyond what is provided by the inputs?

2. Are the input strings contain only ASCII characters, or
are they Unicode strings?  What assumptions can I make
about these input strings?

3. Should we ignore white space?
	- My solutions assume that we should be removing white
	space.  I would still ask the interviewer this question.

# Solution:
This can be a simple function, taking in a string, and
returning a boolean.

This is a two part problem.
- Part one allows us to use
"additional data structures".
- Part two requires that we do not use
"additional data structures"

# Part 1:

If I could use additional data structures, I would select
a language with a hash table / hash map data structure that
disallows duplicate keys.  I'd prefer one that treats an
attempt to insert a duplicate key as an error, or one that
implements something like a contains(key) method.

If the input string were not a valid string (e.g. NULL) I
would either return an error or throw an exception, depending
on the implementation language used.

If the string were empty, I would return true.

Otherwise, if the string was valid, and non-empty, I would
iterate through the string, and attempt to insert each
character as a key in my hash table / map.  The value doesn't
matter here, I only need to see if the hash table / map
contains that key.  If the hash table / map contains the key,
then I would stop processing, and return false, as we know
that the string does not contain all unique characters.

If I reached the end of the string and were able to insert
every character as a key in my hash table / map with no issue,
we would know that the string has all unique characters, so
the function should return true.

This should have linear run time complexity, assuming that the
hash table / map insert() and contains() run in constant time.

# Part 2:

Without additional data structures, the obvious solution is to
compare each character to every other character in the
string.  We will want to avoid comparing characters to
themselves though.  This leads to a quadratic run time
algorithm, with no additional storage space used.

# Implementation:

# Additional notes:
Since my answer for part 2 depends on the answer to question
number 2 above, I decided to go and search for an answer on
Google.

I found an answer on Reddit, however there was a discussion
of the author's solution there.

The comments there seem to agree that
"additional data structures" means no extra storage space
beyond what the input provides.

# References
https://www.reddit.com/r/cscareerquestions/comments/5k1h3g/started_cracking_the_coding_interview/
