const assert = require('assert');
const IsUnique = require('../src/is-unique');

describe('part 1 tests', () => {
  it('Should return true for empty string', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part1(''), true);
  });

  it('Should return true for a string with unique chars.  Single word', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part1('fort'), true);
  });

  it('Should ignore white space (tabs and spaces)', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part1('  fort  and   zul  '), true);
  });

  it('Should return false for string with non-unique chars', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part1('foo bar'), false);
  });

  it('Should return true for string with unique chars (non-ascii)', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part1('привет'), true);
  });

  it('Should return false for string with non-unique chars (non-ascii)', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part1('привет привет'), false);
  });

  it('should throw exception when given non-string input', () => {
    const isUnique = new IsUnique();
    assert.throws(() => {
      isUnique.part1(800);
    }, Error, 'str is not a string!');
  });
});

describe('part 2 tests', () => {
  it('Should return true for empty string', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part2(''), true);
  });

  it('Should return true for a string with unique chars.  Single word', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part2('fort'), true);
  });

  it('Should ignore white space (tabs and spaces)', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part2('  fort  and   zul  '), true);
  });

  it('Should return false for string with non-unique chars', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part2('foo bar'), false);
  });

  it('Should return true for string with unique chars (non-ascii)', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part2('привет'), true);
  });

  it('Should return false for string with non-unique chars (non-ascii)', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.part2('привет привет'), false);
  });

  it('should throw exception when given non-string input', () => {
    const isUnique = new IsUnique();
    assert.throws(() => {
      isUnique.part2(800);
    }, Error, 'str is not a string!');
  });
});

describe('isWhitespace tests', () => {
  it('Should return false for single non-whitespace char', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.isWhitespace('a'), false);
  });

  it('Should return true for single whitespace char', () => {
    const isUnique = new IsUnique();
    assert.equal(isUnique.isWhitespace(' '), true);
  });

  it('should throw exception when given non-string input', () => {
    const isUnique = new IsUnique();
    assert.throws(() => {
      isUnique.isWhitespace(800);
    }, Error, 'str is not a string!');
  });

  it('should throw exception when given string with more than one character', () => {
    const isUnique = new IsUnique();
    assert.throws(() => {
      isUnique.isWhitespace('fort');
    }, Error, 'ch is not a single character!');
  });
});
