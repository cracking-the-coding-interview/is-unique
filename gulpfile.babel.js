const gulp = require('gulp');
const eslint = require('gulp-eslint');
const mocha = require('gulp-mocha');
const runSequence = require('run-sequence');

gulp.task('lint', () =>
  gulp.src(['**/*.js', '!node_modules'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError()));

gulp.task('mocha', () =>
  gulp.src('tests/*.spec.js', { read: false })
    .pipe(mocha()));

gulp.task('test', (done) => {
  runSequence('lint', 'mocha', done);
});

gulp.task('default', ['lint'], () => {
});
